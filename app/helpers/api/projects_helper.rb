# frozen_string_literal: true

module Api
  module ProjectsHelper
    def available_api_projects
      Rails.application.config_for('process')['uc_names']
    end
  end
end
